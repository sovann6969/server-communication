import React, { Component } from 'react';
import './App.css';
import CusMenu from '../menuBar/Menu.jsx'
import Home from '../component/Home'
import Display from '../component/Display'
import About from '../component/About'
import ProtectedRoute from '../route/protectedRoute'
import { BrowserRouter as Router, Switch, Route, useHistory, useLocation ,Redirect} from 'react-router-dom';
import Admin from '../component/Admin';
import CusError from '../component/ErrorPage'
import ViewDetail from '../component/ViewDetail'
import Login from '../auth/login';
import auth from '../auth/Auth'
// import CusForm from '../component/CusForm'

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {

    };
  }

  onG

  render() {
    return (
      <Router>

        <div >
          {/* 
      **header
       */}
          <div className="container-fluid p-0">

            <CusMenu />
            {/* <CusForm /> */}
            {/* 
          ** content
          **
           */}
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/view/:id" children={<ViewDetail/>} />
              <Route path="/display" component={Display} />
              <Route path="/about" component={About} />
              <ProtectedRoute exact path="/admin" children={<Admin />} />
              <Route path="/login" component={Login} />
              <Route component={CusError} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
