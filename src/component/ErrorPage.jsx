import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Auth from '../auth/Auth'
import { useHistory,Link } from 'react-router-dom'
function ErrorPage() {
    const history = useHistory()
    // console.log("erro "+Auth.isAuthenticated())
    return (
        <div className="row d-flex justify-content-center p-5">
            <div className="col-10 col-sm-10- col-md-10 p-5">
                <h1 className="text-center p-5">ស្វែងរកមិនឃើញ  ...</h1>
                <div className="justify-content-center d-flex">
                    <Link className="btn btn-lg btn-primary " to="/" >ត្រលប់ទៅទំព័រដើម</Link>
                </div>
            </div>
        </div>
    )
}

export default ErrorPage
