import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Swal from 'sweetalert2'
import { ToastContainer, toast } from 'react-toastify';
export default class CusForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: '',
            title: '',
            description: '',
            article: []
        }
    }
    componentDidMount() {

        if (this.props.article) {
            this.setState({
                id: this.props.article.ID,
                title: this.props.article.TITLE,
                description: this.props.article.DESCRIPTION
            })
        }
    }
    handlerChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handlerSubmit = e => {
        e.preventDefault()
        if (this.props.isAdd) {
            this.props.onSubmitData(this.state)
        } else {
            Swal.fire({
                title: 'Are you sure you want to update this record?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.value) {
                    this.props.onSubmitData(this.state)

                }
            })

        }
    }




    render() {
        return (
            <div className="col-12 col-md-12 col-sm-12 d-flex justify-content-center">
                <form onSubmit={this.handlerSubmit}>
                    <div className="form-group ">
                        <label htmlFor="title">TITLE</label>
                        <input type="text" className="form-control" style={{ width: "500px" }} onChange={this.handlerChange} id="title" name="title" value={this.state.title} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">DESCRIPTION</label>
                        <input type="text" className="form-control" style={{ width: "500px" }} onChange={this.handlerChange} id="description" name="description" value={this.state.description} />
                    </div>
                    <button type="submit" className="btn btn-success m-1" >{this.props.isAdd ? 'Add' : 'Update'} </button>
                   
                </form>
                <ToastContainer />
            </div>
        )
    }
}
