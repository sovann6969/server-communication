import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import  { Nav, Navbar, Form, FormControl } from 'react-bootstrap';
import {NavLink, Link } from 'react-router-dom';

function Menu() {
  return (
    <div>
      <Navbar bg="light" variant="light">
        <Nav className="mr-auto">
          <NavLink className="mr-5" to="/">ទំព័រដើម</NavLink>
          <NavLink className="mr-5" to="/display">ទំព័របង្ហាញ</NavLink>
          <NavLink className="mr-5" to="/about">អំពីយើងខ្ញុំ</NavLink>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="ស្វែងរក" className="mr-sm-2" />
          <Link to="/admin" className="btn btn-primary btn-sm ">ប្រព័ន្ធប្រតិបត្តិការ</Link>
        </Form>
      </Navbar>
  
    </div>

  );
}
export default Menu;
