import React, { Component } from 'react'
import axios from 'axios'
import { Link, useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
// import { url } from 'inspector';

export default class ViewDetail extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title: '',
            dec: '',
        }
    }
    
    componentDidMount() {
        let st = window.location.href
        let url = st.split("/")
        let id = url[4]
     
        //  console.log(this.props.location.state.sovann)
        axios.get('http://www.api-ams.me/v1/api/articles/' + id)
            .then(res => {
                this.setState({
                    title: res.data.DATA.TITLE,
                    dec: res.data.DATA.DESCRIPTION,
                })
            }).catch(e => console.log(e))
    }
    render() {

        return (
            <div className="col-12 col-sm-12 col-md-12 d-flex justify-content-center py-5">
                <div className="card" >
                    <div className="card-body">
                        <h5 className="card-title">Title : {this.state.title}</h5>
                        <p className="card-text"> <span className="font-weight-bold">Description</span>  : {this.state.dec}</p>

                        <Link to="/admin" className="btn btn-primary btn-sm card-link ">Back to System</Link>
                    </div>
                </div>
            </div>


        )
    }
}