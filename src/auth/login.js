import React from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import auth from './Auth';
import 'bootstrap/dist/css/bootstrap.min.css';
import Img3 from '../img/3.jpg'
function Login() {
  let history = useHistory();
  let location = useLocation();

  let { from } = location.state || { from: { pathname: "/" } };
  let Login = () => {
    auth.authenticate(() => {
      history.replace(from);
    });
  };

  return (
    <div style={{ background: `url(${Img3}) no-repeat center `, backgroundSize: "contain", height: '700px' }}>

      <div className="col-12 col-sm-12 col-md-12 ">
        <h3 className="py-3 d-flex justify-content-center text-danger ">អ្នកត្រូវចុចចូលដើម្បីមើលទំព័រ {from.pathname}</h3>
        <div className="justify-content-center d-flex">
          <button className="btn btn-lg btn-primary " onClick={Login}>ចុចចូល</button>
        </div>
      </div>

    </div>



  );
}
export default Login