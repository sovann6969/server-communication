import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export default class About extends Component {
    render() {
        const Msg = ({ closeToast }) => (
            <div>
              Lorem ipsum dolor
              <button>Retry</button>
              <button onClick={closeToast}>Close</button>
            </div>
          )
        const notify = () => toast.info("Wow so easy !", { autoClose: 7000, position: toast.POSITION.TOP_RIGHT
                                                        , className: 'text-white bg-warning', pauseOnFocusLoss: true });
        return (
           <div>
                    <h3 className="py-4 text-danger font-italic font-weight-light text-center">ទំព័រស្ថិតនៅក្នងការសាកល្បង</h3>
                    <h5 className="text-center">សូមចុចទៅការទំព័រផ្សេង</h5>
                    <button onClick={notify}>Notify !</button>
                    <button onClick={() => toast(<Msg />)}>Hello <span>😀</span></button>
                    <ToastContainer />
             
            </div>
        )
    }
}
