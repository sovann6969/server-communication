import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Img2 from '../img/2.png'
export default class Display extends Component {
    render() {
        return (
            <div>
                <h3 className="py-4 font-italic font-weight-light text-center">គ្មានអ្វីបង្ហាញទេ​ សុំទោស​ <br />​ ​</h3>
                <h5 className="text-center">សូមចុចទៅការទំព័រផ្សេង</h5>

                <div style={{ background: `url(${Img2}) no-repeat center`, backgroundSize: "contain", height: '400px' }}></div>
            </div>
        )
    }
}
