import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import auth from '../auth/Auth'

function ProtectedRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (auth.isAuthenticated) {
          return children
        } else {
          return <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        }

      }

      }
    />
  );
}
export default ProtectedRoute;