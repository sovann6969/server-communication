import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import RenderingTable from './RenderingTable'
import CusForm from './CusForm';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export default class Get extends Component {
  constructor(props) {
    super(props)

    this.state = {
      article: [],
      isAdd: false,
      isUpdate: false,
      isView: false,
      isDel: false,
      id: '',
    }
  }


  handlerAdd = () => {
    this.setState({
      isAdd: true,
      isUpdate: false,
      isView: false,
      isDel: false,
    })
  };
  handlerDel = articleId => {
    axios.delete('http://www.api-ams.me/v1/api/articles/' + articleId)
    this.setState({
      isAdd: false,
      isUpdate: false,
      isView: false,
      isDel: true,
    })

  }
  handlerUpdate = articleId => {
    axios.get(`http://www.api-ams.me/v1/api/articles/` + articleId)
      .then(res => {
        this.setState({
          isAdd: false,
          isUpdate: true,
          isView: false,
          isDel: false,
          article: res.data.DATA
        })
      }).catch(e => console.log(e))
  }
  handlerView = articleId => {
    this.setState({
      isAdd: false,
      isUpdate: false,
      isView: true,
      isDel: false,
      id: articleId
    })
  }
  handlerReredner = () => {
    this.setState({
      isAdd: false,
      isUpdate: false,
      isView: false,
      isDel: false,
    })
  };


  onSubmitData = article => {
    if (this.state.isAdd) {
      axios('http://www.api-ams.me/v1/api/articles', {
        method: 'POST',
        data: { "TITLE": article.title, "DESCRIPTION": article.description }
      }).then(res => 
        this.notif("Record has been Added Successfully"),
        this.setState({
        isAdd: false,
        isUpdate: false,
        isView: false,
      })).catch(e => console.log(e))
    } else {
      axios('http://www.api-ams.me/v1/api/articles/' + article.id, {
        method: 'PUT',
        data: { "TITLE": article.title, "DESCRIPTION": article.description }

      }).then(res => this.notif("Record has been updated Successfully  !"),
        this.setState({
          isAdd: false,
          isUpdate: false,
          isView: false,
        })).catch(e => console.log(e))
    }
  }
  notif = ( msg) =>{
    toast(msg, {
      autoClose: 1500, pauseOnFocusLoss: false,
      position: toast.POSITION.TOP_RIGHT, className: 'rotateY animated', closeButton: false, draggablePercent: 60, type: toast.TYPE.SUCCESS
    })
  }

  render() {
    return (
      <div>
        {this.state.isView ? <Redirect to={{ pathname: "/view/" + this.state.id, state: { id: this.state.id } }} /> :
          (this.state.isUpdate ? <CusForm handlerReredner={this.handlerReredner} article={this.state.article} isUpdate={this.state.isUpdate} onSubmitData={this.onSubmitData} /> :
            (this.state.isAdd ? <CusForm handlerReredner={this.handlerReredner} isAdd={this.state.isAdd} onSubmitData={this.onSubmitData} /> : (this.state.isDel ?
              <RenderingTable isDel={"true"} handlerAdd={this.handlerAdd}
                handlerView={this.handlerView} handlerDel={this.handlerDel} handlerUpdate={this.handlerUpdate} /> : <RenderingTable isDel={"false"} handlerAdd={this.handlerAdd}
                  handlerView={this.handlerView} handlerDel={this.handlerDel} handlerUpdate={this.handlerUpdate} />)))
        }
      </div>

    )
  }
}

